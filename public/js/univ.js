/**
 * Created by konstantin on 27.10.14.
 */

$.each({
    pie: function(view,data,settings){
        new PieChart(this.prepare(view,data,settings));
    },
    time: function(view,data,settings){
        new TimeChart(this.prepare(view,data,settings));
    },
    facet: function(view,data,settings){
        new FacetChart(this.prepare(view,data,settings));
    },
    net: function(view,data,settings){
        //new NetChart(this.prepare(view,data,settings));
        new NetChart({
            container: document.getElementById(view),
            data:{preloaded:data},
            width: settings['width'],
            height: settings['height']
        });
    },
    prepare:function(view,data,settings){
        return {
            container: document.getElementById(view),
            data:{preloaded:{
                unit:settings['unit'],
                values: data,
                from:settings['from'],
                to: settings['to']
            }},
            width: settings['width'],
            height: settings['height']
        }
    }
},$.univ._import);

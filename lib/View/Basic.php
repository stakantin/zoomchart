<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 27.10.14
 * Time: 18:03
 */
namespace stakantin\zoomchart;
class View_Basic extends \View{
    public $data;
    public $settings;
    function init(){
        parent::init();
        Initiator::getInstance()->addLocation($this->app);
        $this->owner->app->jquery->addStaticInclude('zoomcharts');
        $this->owner->app->jquery->addStaticInclude('univ');
    }
}
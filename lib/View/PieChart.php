<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 26.10.14
 * Time: 23:06
 */
namespace stakantin\zoomchart;
class View_PieChart extends View_Basic{
    function init(){
        parent::init();
        $this->js(true)->univ()->pie($this->name,$this->data,$this->settings);
    }
}
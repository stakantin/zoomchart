Zoomcharts addon for ATK4.3
===========================

#Installation

Use composer.json

    {
        "require":{
            "stakantin\/zoomchart":"dev-master"
        }
    }

Clone it from https://bitbucket.org/stakantin/zoomchart

#How to use

Just add a View (you need to prepare proper data, see https://zoomcharts.com/en/documentation/ for references)

    $this->add('stakantin\zoomchart\View_TimeChart',[
                'data'   =>$data,
                'settings'=>[]
            ]);
    or
    $this->add('stakantin\zoomchart\View_PieChart',[
                'data'   =>$data,
                'settings'=>[]
            ]);
    or
    $this->add('stakantin\zoomchart\View_NetChart',[
                'data'   =>$data,
                'settings'=>[]
            ]);
    or
    $this->add('stakantin\zoomchart\View_FacetChart',[
                'data'   =>$data,
                'settings'=>[]
            ]);